import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/listing',
    name: 'Listing',
    
    component: () => import('../views/Listing.vue')
  },
  {
    path: '/new',
    name: 'NewListe',
    
    component: () => import('../views/NewListe.vue')
  },
  {
    path: '/list/:id',
    name: 'liste',
    
    component: () => import('../views/ListeItemById.vue')
  },
  {
    path: '/list/:id/add',
    name: 'addItem',
    
    component: () => import('../views/AddItemByListId.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
